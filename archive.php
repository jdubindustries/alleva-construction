<?php
/**
 * The template for displaying archive pages ( Blog / Case Studies )
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package socius_custom
 */

get_header(); ?>

<!-- Hero Internal -->

<?php get_template_part( 'template-parts/content', 'herointernal' ); ?>

<!-- Main Content -->

<div id="main-content" class="bgsoftgrey" role="main">
    <!-- Home Content Top Starts -->
    <div class="row">
        <div class="col-sm-8 nopadding bgwhite">
            <div class="contentArea">
              <?php
          		if ( have_posts() ) : ?>

          			<header class="page-header">
          				<?php
          					the_archive_title( '<h1 class="page-title">', '</h1>' );
          					the_archive_description( '<div class="archive-description">', '</div>' );
          				?>
          			</header><!-- .page-header -->

          			<?php
          			/* Start the Loop */
          			while ( have_posts() ) : the_post();

          				/*
          				 * Include the Post-Format-specific template for the content.
          				 * If you want to override this in a child theme, then include a file
          				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
          				 */
          				get_template_part( 'template-parts/content', get_post_format() );

          			endwhile;

          			the_posts_navigation();

          		else :

          			get_template_part( 'template-parts/content', 'none' );

          		endif; ?>
            </div>
        </div>
        <div class="col-sm-4 sidebar nopadding bgsoftgrey">
            <?php get_template_part( 'template-parts/content', 'sidebar-top' ); ?>
        </div>
    </div>
</div>


<!-- About Us -->
<?php get_template_part( 'template-parts/content', 'about' ); ?>

<!-- Gallery -->
<a name="gallery"></a>
<?php get_template_part( 'template-parts/content', 'gallery' ); ?>

<!-- Get Quote -->
<?php get_template_part( 'template-parts/content', 'quote' ); ?>

<!-- Footer -->
<?php get_footer(); ?>
