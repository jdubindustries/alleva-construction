<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package socius_custom
 */
?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage">

    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="icon" href="<?php bloginfo('url'); ?>/favicon.ico" type="image/x-icon">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div id="page" class="site full">
            <?php get_template_part( 'template-parts/content', 'block-header' ); ?>
            <?php if ( is_front_page() ) {
              get_template_part( 'template-parts/content', 'home-hero' );
            } else {
                get_template_part( 'template-parts/content', 'internal-hero' );
            }; ?>
            <?php
                get_template_part( 'template-parts/content', 'block-quickform' );

            ?>
            <?php if (is_front_page() ) {
                get_template_part( 'template-parts/content', 'home-ctas' );
            } ?>

            <?php if ((! is_front_page() ) &&
            ((is_page_template('page-baths.php')) ||
            (is_page_template('page-kitchens.php')) ||
            (is_page_template('page-masonry.php')) ||
            (is_page_template('page-additions.php')))) {
              get_template_part( 'template-parts/content', 'block-steps' );
              get_template_part( 'template-parts/content', 'block-promotions' );
            } ?>

            <?php if(is_home()) { ?>
            <div id="content" class="site-content container">
            <?php } else { ?>
            <div id="content" class="site-content full">
            <?php } ?>
