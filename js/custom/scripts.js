
jQuery(function($) {
  $(document).ready(function() {
    $('a[href*="tel"]').on('click', function() {
      ga('send', 'event', 'Phone', 'Mobile Click', 'Mobile Phone Click Tracking');
    });
    $("#mobilemenu").mmenu();
    $(function() {
      $('.step').matchHeight(options);
    });
    //Lazy load various element types
    $("img.lazy, span.lazy, div.lazy").lazyload({
        threshold : 50,
        effect : "fadeIn",
    });
    /* Wufoo Form */
    var options = {
    	classNamePrefix: 'bvalidator_red_',
    	position: {x:'left', y:'top'},
    	validClass: true,
    	validateOn: 'keyup'
    }
    $('.wufoo').bValidator(options);
    $('.phone_us').mask('(000) 000-0000');
    /* Promotions Slider */
    $('.promotions').slick({
      dots: true,
      prevArrow: $('.promotions-wrapper .prev'),
      nextArrow: $('.promotions-wrapper .next'),
    });
    /* Reviews Slider */
    $('.reviews').slick({
      arrows: true,
      prevArrow: $('.reviews-wrapper .prev'),
      nextArrow: $('.reviews-wrapper .next'),
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1366,
          settings: {
            arrows: true,
            prevArrow: $('.reviews-wrapper .prev'),
            nextArrow: $('.reviews-wrapper .next'),
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 992,
          settings: {
            arrows: true,
            prevArrow: $('.reviews-wrapper .prev'),
            nextArrow: $('.reviews-wrapper .next'),
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
    /* Product Galleries */
    $('.product .slider').slick({
      autoplay: false,
      dots: false,
      arrows: false,
      infinite: true,
      asNavFor: '.product .slider-nav',
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: true,
            prevArrow: $('.slider-wrapper .prev'),
            nextArrow: $('.slider-wrapper .next'),
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
    $('.product .slider-nav').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      vertical: true,
      infinite: true,
      focusOnSelect: true,
      asNavFor: '.product .slider',
      prevArrow: $('.slider-nav-wrapper .prev'),
      nextArrow: $('.slider-nav-wrapper .next'),
    });
    /* About Gallery */
    $('.homeabout .slider').slick({
      autoplay: false,
      dots: false,
      arrows: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      asNavFor: '.homeabout .slider-nav',
    });
    $('.homeabout .slider-nav').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      vertical: true,
      infinite: true,
      focusOnSelect: true,
      asNavFor: '.homeabout .slider',
    });
    show_sticky_form();
  });
  var n = 0;
  function show_sticky_form() {
		if ( $(window).width() > 1200 && n == 0) {
			// Sticky nav on internal pages
			$(window).scroll(function(){
        var window_top = jQuery(window).scrollTop() - 10;
        var div_top = jQuery('.form-target').offset().top;
        var ScrollTop = jQuery(window).scrollTop();
            if (window_top > div_top) {
                jQuery('.quick-form').addClass('stick');
            } else {
                jQuery('.quick-form').removeClass('stick');
            }
			});
    };
	};
});
