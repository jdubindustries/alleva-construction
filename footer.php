<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package socius_custom
 */

?>

</div><!-- #content -->
<?php if ((! is_page_template('page-baths.php')) &&
(! is_page_template('page-kitchens.php')) &&
(! is_page_template('page-masonry.php')) &&
(! is_page_template('page-additions.php'))) {
	get_template_part( 'template-parts/content', 'block-promotions' );
} if (is_front_page()) {
	get_template_part( 'template-parts/content', 'home-aboutgallery' );
	get_template_part( 'template-parts/content', 'home-homemakeover' );
} ?>
<?php get_template_part( 'template-parts/content', 'block-productgallery' ); ?>
<?php if (! is_page_template('page-reviews.php')) { get_template_part( 'template-parts/content', 'block-reviews' ); } ?>
<?php get_template_part( 'template-parts/content', 'block-footercta' ); ?>
<?php get_template_part( 'template-parts/content', 'block-trust' ); ?>
<?php get_template_part( 'template-parts/content', 'block-footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

<!-- MMenu Mobile -->
<?php get_template_part( 'template-parts/content', 'mobilemenu' ); ?>

</body>
</html>
