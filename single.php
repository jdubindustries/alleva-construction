<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package socius_custom
 */

get_header(); ?>

<!-- Hero Internal -->

<?php get_template_part( 'template-parts/content', 'herointernal' ); ?>

<!-- Main Content -->

<div id="main-content" class="bgsoftgrey" role="main">
    <!-- Home Content Top Starts -->
    <div class="row">
        <div class="col-sm-8 nopadding bgwhite">
            <div class="contentArea">
                <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', get_post_format() );

                    //the_post_navigation();

                    /*
                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;
                    */

                endwhile; // End of the loop.
                ?>
                <div class="internal-cta">
                    <?php dynamic_sidebar( 'sidebar-internal' ); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-4 sidebar nopadding bgsoftgrey">
            <?php get_template_part( 'template-parts/content', 'sidebar-top' ); ?>
        </div>
    </div>
</div>


<!-- About Us -->
<?php get_template_part( 'template-parts/content', 'about' ); ?>

<!-- Gallery -->
<a name="gallery"></a> 
<?php get_template_part( 'template-parts/content', 'gallery' ); ?>

<!-- Get Quote -->
<?php get_template_part( 'template-parts/content', 'quote' ); ?>

<!-- Footer -->
<?php get_footer(); ?>