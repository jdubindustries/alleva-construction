<div class="footercta container">
  <div class="footercta-header">
    <?php echo the_field('trust_cta_headline','option'); ?>
  </div>
  <a href="<?php echo get_field('trust_cta_link', 'option'); ?>" class="btn btn-primary">
    <?php echo get_field('trust_cta_button_text', 'option'); ?>
  </a>
</div>
