<?php if( have_rows('trust_images', 'option') ): ?>
  <div class="trust container">
      <?php while( have_rows('trust_images', 'option') ): the_row();
          $img = get_sub_field('trust_image', 'option');
          $url = $img['url'];
          $alt = $img['alt'];
      ?>
        <div class="trust-img text-center">
          <img class="lazy" src="<?php bloginfo( 'template_directory' );?>/images/dummy.png" data-original="<?php echo $url; ?>" alt="<?php echo $alt; ?>">
        </div>
      <?php endwhile; ?>
  </div>
<?php endif; ?>
