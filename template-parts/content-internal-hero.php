<!-- Internal Hero -->
<?php

  $bg_image = get_bloginfo('stylesheet_directory').'/images/headers/header-'.rand(1,5).'.jpg';

  $category = get_field( 'product_category' );
  $product_category = $category['value'];

  if(is_page_template("page-baths.php")) {
    $bg_image = get_bloginfo('stylesheet_directory').'/images/headers/Bathroom.jpg';
  } else if(is_page_template("page-additions.php")) {
    $bg_image = get_bloginfo('stylesheet_directory').'/images/headers/Additions.jpg';
  } else if(is_page_template("page-masonry.php")) {
    $bg_image = get_bloginfo('stylesheet_directory').'/images/headers/Masonry.jpg';
  } else if(is_page_template("page-repairs.php")) {
    $bg_image = get_bloginfo('stylesheet_directory').'/images/headers/Repair.jpg';
  }  else if(is_page_template("page-kitchens.php")) {
    $bg_image = get_bloginfo('stylesheet_directory').'/images/headers/Kitchen.jpg';
  }    

  else if(!is_home() && !is_single() && !is_archive() && !is_404() && has_post_thumbnail()) {
    $bg_image = get_the_post_thumbnail_url(); 
  }

  // $my_title = get_field( 'page_title' );
  // if ($my_title) {
  //   $the_title = $my_title;
  // } 

?>


  <div class="lazy hero-internal" style="background-image:url(<?php bloginfo('template_directory');?>/images/dummy.png)" data-original="<?php echo $bg_image; ?>">
    
  </div>

