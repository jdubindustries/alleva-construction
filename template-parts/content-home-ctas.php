<!-- Home CTAs -->

<?php 
  $ctaimage1 = get_field('ctaimage_kitchens', 'option'); 
  $ctaimage2 = get_field('ctaimage_baths', 'option'); 
  $ctaimage3 = get_field('ctaimage_additions', 'option'); 
  $ctaimage4 = get_field('ctaimage_masonry', 'option'); 
  $ctaimage5 = get_field('ctaimage_repairs', 'option'); 
?>

<div class="homeCTAs">
    <div class="section-header h1"><?php echo get_field('cte_header','option'); ?></div>
    <div class="section-subheader"><?php echo get_field('cta_sub_header','option'); ?></div>
    <div class="container">
      <div class="homeCTAs-wrapper">
        <div class="homeCTA-row homeCTA-row-1">

          <div class="lazy homeCTA" style="background-image:url(<?php bloginfo('template_directory');?>/images/dummy.png); ?>;" data-original="<?php echo $ctaimage1['sizes']['home-cta']; ?>">
            <div class="img-hover">
              <a class="img-link" href="<?php echo get_field('ctalink_kitchens', 'option'); ?>" role="button">
                <div class="ctatitle">Kitchens</div>
                <div class="ctatext"><?php echo get_field('ctatext_kitchens', 'option'); ?></div>
                  <div class="btn btn-primary">Learn More</div>
                <div class="overlay">
                  
                </div>
              </a>
            </div>
          </div>

          <div class="lazy homeCTA" style="background-image:url(<?php bloginfo('template_directory');?>/images/dummy.png); ?>;" data-original="<?php echo $ctaimage2['sizes']['home-cta']; ?>">
            <div class="img-hover">
              <a class="img-link" href="<?php echo get_field('linkctalink_baths', 'option'); ?>" role="button">
                <div class="ctatitle">Baths</div>
                <div class="ctatext"><?php echo get_field('textctatext_baths', 'option'); ?></div>
                  <div class="btn btn-primary">Learn More</div>
                <div class="overlay">
                  
                </div>
              </a>
            </div>
          </div>

          <div class="lazy homeCTA" style="background-image:url(<?php bloginfo('template_directory');?>/images/dummy.png); ?>;" data-original="<?php echo $ctaimage3['sizes']['home-cta']; ?>">
            <div class="img-hover">
              <a class="img-link" href="<?php echo get_field('linkctalink_additions', 'option'); ?>" role="button">
                <div class="ctatitle">Additions</div>
                <div class="ctatext"><?php echo get_field('textctatext_additions', 'option'); ?></div>
                  <div class="btn btn-primary">Learn More</div>
                <div class="overlay">
                  
                </div>
              </a>
            </div>
          </div>
        </div>

        <div class="homeCTA-row homeCTA-row-2">
           <div class="lazy homeCTA" style="background-image:url(<?php bloginfo('template_directory');?>/images/dummy.png); ?>;" data-original="<?php echo $ctaimage4['sizes']['home-cta']; ?>">
            <div class="img-hover">
              <a class="img-link" href="<?php echo get_field('linkctalink_masonry', 'option'); ?>" role="button">
                <div class="ctatitle">Masonry</div>
                <div class="ctatext"><?php echo get_field('textctatext_masonry', 'option'); ?></div>
                <div class="btn btn-primary">Learn More</div>
                <div class="overlay"></div>
              </a>
            </div>
          </div>

          <div class="lazy homeCTA" style="background-image:url(<?php bloginfo('template_directory');?>/images/dummy.png); ?>;" data-original="<?php echo $ctaimage5['sizes']['home-cta']; ?>">
            <div class="img-hover">
              <a class="img-link" href="<?php echo get_field('linkctalink_repairs', 'option'); ?>" role="button">
                <div class="ctatitle">Repairs</div>
                <div class="ctatext"><?php echo get_field('textctatext_repairs', 'option'); ?></div>
                <div class="btn btn-primary">Learn More</div>
                <div class="overlay"></div>
              </a>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
