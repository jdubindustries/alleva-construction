<?php
  if (is_page_template('page-kitchens.php')):
    $gallery_header = get_field('gallery_header_kitchen','option');
    $gallery_subheader = get_field('gallery_subheader_kitchen','option');
    $images = get_field('kitchens_gallery', 'option');
  elseif (is_page_template('page-additions.php')):
    $gallery_header = get_field('gallery_header_additions','option');
    $gallery_subheader = get_field('gallery_subheader_additions','option');
    $images = get_field('additions_gallery', 'option');
  elseif (is_page_template('page-baths.php')):
    $gallery_header = get_field('gallery_header_baths','option');
    $gallery_subheader = get_field('gallery_subheader_baths','option');
    $images = get_field('baths_gallery', 'option');
  elseif (is_page_template('page-masonry.php')):
    $gallery_header = get_field('gallery_header_masonry','option');
    $gallery_subheader = get_field('gallery_subheader_masonry','option');
    $images = get_field('masonry_gallery', 'option');
  elseif (is_page_template('page-masonry.php')):
    $gallery_header = get_field('gallery_header_masonry','Dption');
    $gallery_subheader = get_field('gallery_subheader_masonry','option');
    $images = get_field('masonry_gallery', 'option');
  elseif (is_page_template('page-repairs.php')):
    $gallery_header = get_field('gallery_header_repairs','option');
    $gallery_subheader = get_field('gallery_subheader_repairs','option');
    $images = get_field('repairs_gallery', 'option');
  else:
    $gallery_header = get_field('gallery_header_default','option');
    $gallery_subheader = get_field('gallery_subheader_default','option');
    $images = get_field('default_gallery', 'option');
  endif;
  ?>
  <?php if( $images ): ?>
  <div id="product-gallery" class="product gallery container">
    <div class="gallery-container">
    	<div class="gallery-header section-header h1"><?php echo $gallery_header; ?></div>
    	<div class="section-subheader"><?php echo $gallery_subheader; ?></div>
    	<br>

      <div class="slider-wrapper">
      	<div class="slider">
          <?php foreach( $images as $image ): ?>
  	        <div class="slider-img" style="background-image:url(<?php echo $image['sizes']['gallery']; ?>);"></div>
        	<?php endforeach; ?>
        </div>
        <div class="prev arrow"><img src="<?php echo get_template_directory_uri(); ?>/images/arrow-red-left.png" class="lazy"></div>
        <div class="next arrow"><img src="<?php echo get_template_directory_uri(); ?>/images/arrow-red-right.png" class="lazy"></div>
      </div>
      <div class="slider-nav-wrapper">
        <div class="prev arrow"><img src="<?php echo get_template_directory_uri(); ?>/images/arrow-red-top.png" class="lazy"></div>
        <div class="slider-nav">
          	<?php foreach( $images as $image ): ?>
      	      <div class="slider-nav-scroll"><img src="<?php echo $image['sizes']['gallery-thumb']; ?>" alt="<?php echo $image['alt']; ?>"></div>
            <?php endforeach; ?>
        </div>
        <div class="next arrow"><img src="<?php echo get_template_directory_uri(); ?>/images/arrow-red-bottom.png" class="lazy"></div>
     </div>
   </div>
  </div>
<?php endif;  ?>
