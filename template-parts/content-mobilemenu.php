<nav id="mobilemenu" class="mobilemenu">
	<?php //Stitch together menus
		$topmenu = wp_nav_menu( array (  'echo' => false, 'theme_location' => 'top-menu', 'items_wrap' => '%3$s' , 'container' => false));
		$mainmenu = wp_nav_menu( array (  'echo' => false, 'theme_location' => 'main-menu', 'items_wrap' => '%3$s' , 'container' => false));
		echo '<ul id="mobile-menu">'. $mainmenu . $topmenu . '</ul>';
	?>
</nav>