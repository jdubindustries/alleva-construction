<div class="promotions-wrapper">
  <div class="promotions">

    <?php

    // check if the repeater field has rows of data
  	if( have_rows('new_promotions', 'option') ):

  	 	// loop through the rows of data
  	  while ( have_rows('new_promotions', 'option') ) : the_row();

        $header1 = null;
        $header2 = null;
        $header3 = null;
        $subheader = null;
        $buttonlink = null;
        $buttontext = null;
        $banner = null;
        // $associated_product = get_sub_field('the_product');
        // $product_category = $associated_product['value'];

        if (is_page_template('page-kitchens.php')) {
          $product = 'kitchens';
        } else if (is_page_template('page-baths.php')) {
          $product = 'baths';
        } else if (is_page_template('page-additions.php')) {
          $product = 'additions';
        } else if (is_page_template('page-masonry.php')) {
          $product = 'masonry';
        } //else if (is_page_template('page-repairs.php')) {
        //   $product = 'repairs';
        // }

        if (( empty($product) ) && ( get_sub_field('featured_promotion'))):

          $header1 = get_sub_field( "header_1" );
          $header2 = get_sub_field( "header_2" );
          $header3 = get_sub_field( "header_3" );
          $subheader = get_sub_field( "sub_header" );
          $buttonlink = get_sub_field("button_link");
          $buttontext = get_sub_field("button_text");
          $banner = get_sub_field('banner');

        elseif ( get_row_layout() == $product ) :

          $header1 = get_sub_field( "header_1" );
          $header2 = get_sub_field( "header_2" );
          $header3 = get_sub_field( "header_3" );
          $subheader = get_sub_field( "sub_header" );
          $buttonlink = get_sub_field("button_link");
          $buttontext = get_sub_field("button_text");
          $banner = get_sub_field('banner');

        endif;

        ?>

  				<?php if( !empty($banner) ): ?>
            <div class="promotion">
                <div class="lazy promotion-bg" style="background-image:url(<?php bloginfo('template_directory');?>/images/dummy.png); ?>;" data-original="<?php echo $banner['url']; ?>">
                  <div class="promotion-innerwrapper">
                    <div class="promotion-header-small"><?php echo $header1; ?></div>
                    <div class="promotion-header-large"><?php echo $header2; ?></div>
                    <div class="promotion-header-small"><?php echo $header3; ?></div>
                    <div class="promotion-header-sub hidden-xs"><?php echo $subheader; ?></div>
                    <a class="btn btn-primary hidden-xs" href="<?php echo the_sub_field("button_link"); ?>" role="button">
                      <?php echo the_sub_field("button_text"); ?>
                    </a>
                  </div>
                  <div class="promotion-color"></div>
                </div>

                <div class="promotion-header-sub visible-xs"><?php echo $subheader; ?></div>
                <a class="btn btn-primary visible-xs" href="<?php echo the_sub_field("button_link"); ?>" role="button">
                  <?php echo the_sub_field("button_text"); ?>
                </a>
            </div>
          <?php endif; ?>

      	<?php

        // Check if enabled for homepage

   		endwhile;

    endif;

    ?>
  </div>
  <div class="promotions-bxshdw"></div>
  <div class="lazy prev"><img src="<?php echo get_template_directory_uri(); ?>/images/arrow-left.png" class="lazy" alt="Left"></div>
  <div class="lazy next"><img src="<?php echo get_template_directory_uri(); ?>/images/arrow-right.png" class="lazy" alt="Right"></div>
</div>
