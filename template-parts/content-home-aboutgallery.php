<?php

    $aboutheader = get_field('about_header');
    $abouttext = get_field('about_text');
    $images = get_field('gallery');

  ?>
  <?php if( $images ): ?>
  <div class="homeabout container">

    <div class="gallery">

      <div class="gallery-header section-header h1 hidden-lg"><?php echo $aboutheader; ?></div>

      <div class="gallery-container">
        <div class="slider-wrapper">

        	<div class="slider">
            <?php foreach( $images as $image ): ?>
    	        <div class="slider-img" style="background-image:url(<?php echo $image['sizes']['gallery']; ?>);">
               <div class="img-title"><?php echo $image['title']; ?></div>
              </div>
          	<?php endforeach; ?>
          </div>

        </div>
        <div class="slider-nav-wrapper">

          <div class="slider-nav">
            	<?php foreach( $images as $image ): ?>
        	      <div class="slider-nav-scroll"><img src="<?php echo $image['sizes']['gallery-thumb']; ?>" alt="<?php echo $image['alt']; ?>"></div>
              <?php endforeach; ?>
          </div>

       </div>
     </div>
   </div>
     <?php if( $abouttext ): ?>
        <div class="about-section">
          <div class="gallery-header h1 hidden-xs hidden-sm hidden-md"><?php echo $aboutheader; ?></div>
          <div class="about-text"><?php echo $abouttext ?></div>
        </div>
      <?php endif;  ?>
  </div>
<?php endif;  ?>
