<div class="reviews-header section-header h1">
  <a href="<?php echo get_permalink(562); ?>">Client Satisfaction is Our Priority</a>
</div>

<div class="reviews-wrapper">
  <div class="reviews container">

    <?php
  	// check if the repeater field has rows of data
  	if( have_rows('testimonial', 'option') ):

  	 	// loop through the rows of data
  	  while ( have_rows('testimonial', 'option') ) : the_row();
        $givchars = 225;
        $postgiv = get_sub_field( "testimonial_text" );
        $modgiv = substr($postgiv, 0, $givchars);
        $name = get_sub_field( "testimonial_name" );

        ?>

            <div class="review">
              <div class="review-wrapper">
                <a href="<?php echo get_permalink(562); ?>">
                  <div class="review-stars"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></div>
                  <div class="review-text">&quot;<?php echo $modgiv; ?>&quot;</div>
                  <div class="review-name"><b>&ndash; <?php echo $name; ?></b></div>
                </a>
              </div>
            </div>

      	<?php

   		endwhile;

    endif;

    ?>

  </div>
  <div class="prev hidden-xs"><img src="<?php echo get_template_directory_uri(); ?>/images/arrow-red-left.png" class="lazy" alt="Left"></div>
  <div class="next hidden-xs"><img src="<?php echo get_template_directory_uri(); ?>/images/arrow-red-right.png" class="lazy" alt="Right"></div>
</div>
