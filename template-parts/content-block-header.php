<!-- Header -->
<header id="masthead" class="site-header">
  <div class="container header-container">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="header-logo">
      <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Alleva Construction, Inc." />
    </a>
    <div class="desktop-break">
      <div class="top-menu hidden-xs hidden-sm"><?php wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?></div>
      <div class="tablet-break">
        <div class="phone">
          <div class="phone-icon">
            <a href="tel:1-<?php the_field('phone_number','option'); ?>" title="Call Alleva">
              <img src="<?php echo get_template_directory_uri(); ?>/images/icon-phone.png" alt="Call Us" />
            </a>
          </div>
          <div class="phone-number text-center hidden-xs">
            <a href="tel:1-<?php the_field('phone_number','option'); ?>" title="Call Alleva"><?php the_field('phone_number','option'); ?></a>
          </div>
        </div>
        <div class="mobile-menu visible-xs visible-sm">
          <a href="#mobilemenu"><i class="fa fa-bars" aria-hidden="true"></i></a>
        </div>
      </div>
    </div>
    <div id="navigation" class="main-navigation visible-md visible-lg">
      <nav id="site-navigation" class="site-navigation" role="navigation">
        <?php wp_nav_menu(array('theme_location' => 'main-menu','container' => false,'menu_class' => 'main-menu', )); ?>
      </nav>
    </div>
  </div>
</header>
