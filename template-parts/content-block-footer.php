<div class="footer">
    <div class="container">
          <div class="footer-contact hidden-xs hidden-sm col-md-12 hidden-lg">
            <ul>
              <li class="address"><b>Office:</b> <?php the_field('address','option'); ?></li>
              <li class="phone"><b>Phone:</b> <a href="tel:1-<?php the_field('phone_number','option'); ?>" title="Call Alleva"><?php the_field('phone_number','option'); ?></a></li>
              <li class="phone"><b>Fax:</b> <?php the_field('fax_number','option'); ?></li>
              <li class="email"><b>Email:</b> <a href="mailto:<?php the_field('contact_email','option'); ?>"><?php the_field('contact_email','option'); ?></a></li>
            </ul>
          </div>
          <div class="logo-social col-xs-12">
            <div class="col-xs-12">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="footer-logo">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Alleva Construction, Inc." />
              </a>
            </div>
            <div class="social-icons col-xs-12">
                <ul>
                    <?php if( have_rows('social_media','option') ): ?>
                      <?php while ( have_rows('social_media','option') ) : the_row();?>
                        <?php
                          $img = get_sub_field('social_media_icon', 'option');
                          $url = $img['url'];
                        ?>
                        <li>
                          <a href="<?php the_sub_field('social_media_link','option'); ?>" target="_blank">
                            <img class="lazy" src="<?php bloginfo( 'template_directory' );?>/images/dummy.png" data-original="<?php echo $url; ?>" alt="<?php echo $alt; ?>">
                          </a>
                        </li>
                      <?php endwhile;
                    endif; ?>
                </ul>
            </div>
          </div>
          <div class="footer-nav col-xs-12">
            <div class="hidden-xs hidden-sm">
              <?php wp_nav_menu( array( 'footer-menu' => '', 'menu_class' => 'footer-menu' , 'container_class' => '', 'theme_location' => 'footer-menu' ) ); ?>
            </div>
            <div class="clearfix"></div>
            <div class="fine">&copy; 2008-<?php echo date("Y"); ?> Alleva Construction Inc.
              <br class="visible-xs" />
              All rights reserved. <a href="<?php echo get_permalink(76); ?>">Privacy Policy</a>
              <?php $area_terms = get_terms( array('taxonomy' => 'smct_areas','hide_empty' => true) );
                if(!empty( $area_terms) && !is_wp_error( $area_terms )) { ?>
                  | <a href="<?php echo get_permalink(10); ?>"> Area Served</a>
              <?php } ?>
            </div>
          </div>
          <div class="footer-contact hidden-xs hidden-sm hidden-md">
            <ul>
              <li class="address"><b>Office:</b> <?php the_field('address','option'); ?></li>
              <li class="phone"><b>Phone:</b> <a href="tel:1-<?php the_field('phone_number','option'); ?>" title="Call Alleva"><?php the_field('phone_number','option'); ?></a></li>
              <li class="phone"><b>Fax:</b> <?php the_field('fax_number','option'); ?></li>
              <li class="email"><b>Email:</b> <a href="mailto:<?php the_field('contact_email','option'); ?>"><?php the_field('contact_email','option'); ?></a></li>
            </ul>
          </div>
    </div>
</div>
