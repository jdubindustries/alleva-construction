<?php
  echo '<div class="hmgallery-container container"><div class="section-header h1">' . get_field('home_makeover_gallery_title','option') . '</div>';
  // check if the repeater field has rows of data
  if( have_rows('home_makeover_gallery', 'option') ):
    $count = 0;
    $group = 0;
    // loop through the rows of data

    while ( have_rows('home_makeover_gallery', 'option') ) : the_row();

    $title = get_sub_field('title');
    $link = get_sub_field('link');
    $image = get_sub_field('image');
    if ($count % 2 == 0) {
      $group++;
      ?>
        <div id="hmgallery-<?php echo $group; ?>" class="hmgallery-row hmgallery-<?php echo $group; ?>">
      <?php
        }
      ?>

    <div class="lazy hmgallery" style="background-image:url(<?php bloginfo('template_directory'); ?>/images/dummy.png); ?>;" data-original="<?php echo $image['sizes']['makeover-gallery']; ?>">
      <div class="hmcover">
        <a href="<?php echo $link; ?>#product-gallery">
          <?php echo $title; ?>
        </a>
      </div>
      <div class="h2 visible-lg-inline"><?php echo $title; ?></div>
      <div class="hmcolor"></div>
    </div>

    <?php
    if ($count % 2 == 1) {
        ?>
      </div><!-- .hmgallery-row -->
        <?php
      }
      $count++;
    endwhile;

  endif;
  echo '</div></div><!-- .hmgallery-container -->';
?>
