<?php 
  if (is_page_template('page-kitchens.php')):
    $main_header = get_field('title','option');
    $sub_header = get_field('sub_title','option');
    $step1_header = get_field('step_1_title','option');
    $step1_text = get_field('step_1_text','option');
    $step2_header = get_field('step_2_title','option');
    $step2_text = get_field('step_2_text','option');
    $step3_header = get_field('step_3_title','option');
    $step3_text = get_field('step_3_text','option');
  elseif (is_page_template('page-baths.php')):
    $main_header = get_field('title_baths','option');
    $sub_header = get_field('sub_title_baths','option');
    $step1_header = get_field('step_1_title_baths','option');
    $step1_text = get_field('step_1_text_baths','option');
    $step2_header = get_field('step_2_title_baths','option');
    $step2_text = get_field('step_2_text_baths','option');
    $step3_header = get_field('step_3_title_baths','option');
    $step3_text = get_field('step_3_text_baths','option');
  elseif (is_page_template('page-additions.php')):
    $main_header = get_field('title_additions','option');
    $sub_header = get_field('sub_title_additions','option');
    $step1_header = get_field('step_1_title_additions','option');
    $step1_text = get_field('step_1_text_additions','option');
    $step2_header = get_field('step_2_title_additions','option');
    $step2_text = get_field('step_2_text_additions','option');
    $step3_header = get_field('step_3_title_additions','option');
    $step3_text = get_field('step_3_text_additions','option');
  elseif (is_page_template('page-masonry.php')):
    $main_header = get_field('title_masonry','option');
    $sub_header = get_field('sub_title_masonry','option');
    $step1_header = get_field('step_1_title_masonry','option');
    $step1_text = get_field('step_1_text_masonry','option');
    $step2_header = get_field('step_2_title_masonry','option');
    $step2_text = get_field('step_2_text_masonry','option');
    $step3_header = get_field('step_3_title_masonry','option');
    $step3_text = get_field('step_3_text_masonry','option');
  elseif (is_page_template('page-repairs.php')):
    $main_header = get_field('title_repairs','option');
    $sub_header = get_field('sub_title_repairs','option');
    $step1_header = get_field('step_1_title_repairs','option');
    $step1_text = get_field('step_1_text_repairs','option');
    $step2_header = get_field('step_2_title_repairs','option');
    $step2_text = get_field('step_2_text_repairs','option');
    $step3_header = get_field('step_3_title_repairs','option');
    $step3_text = get_field('step_3_text_repairs','option');
  endif; ?>
<?php if( $main_header ): ?>
  <div class="steps container">
    <div class="h1"><?php echo $main_header; ?></div>
    <div class="section-subheader"><?php echo $sub_header; ?></div>
    <?php if ( $step1_header ): ?>
      <div class="step col-xs-12 col-sm-6 col-md-4">
        <img class="lazy" src="<?php echo get_template_directory_uri(); ?>/images/step-01.png">
        <div class="h2"><?php echo $step1_header; ?></div>
        <div class="section-subheader"><?php echo $step1_text; ?></div>
      </div>
      <div class="step col-xs-12 col-sm-6 col-md-4">
        <img class="lazy" src="<?php echo get_template_directory_uri(); ?>/images/step-02.png">
        <div class="h2"><?php echo $step2_header; ?></div>
        <div class="section-subheader"><?php echo $step2_text; ?></div>
      </div>
      <div class="step col-xs-12 col-md-4">
        <img class="lazy" src="<?php echo get_template_directory_uri(); ?>/images/step-03.png">
        <div class="h2"><?php echo $step3_header; ?></div>
        <div class="section-subheader"><?php echo $step3_text; ?></div>
      </div>
    <?php endif; ?>
  </div>
<?php endif; ?>
