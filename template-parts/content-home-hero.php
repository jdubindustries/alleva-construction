<!-- Home Hero -->
<?php
  $img = get_field('background_image');
  $url = $img['url'];
?>
<div class="hero" style="background-image:url(<?php echo $url; ?>)">
  <div class="hero-color">
    <div class="hero-head"><?php echo get_field('headline_1'); ?></div>
    <div class="hero-head hero-head-2"><?php echo get_field('headline_2'); ?></div>
    <div class="hero-subtext"><?php echo get_field('sub_headline'); ?></div>
    <a href="<?php echo get_field('page_link'); ?>" class="btn btn-primary"><?php echo get_field('button_text'); ?></a>
    <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down.png" alt="Continue" class="hidden-lg" />
  </div>
  <div class="hero-skew"></div>
  <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down.png" alt="Continue" class="visible-lg hero-down" />
</div>
