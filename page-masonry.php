<?php
/**
 * Template Name: Masonry
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

<!-- Main Content -->

<div id="main-content" class=" container" role="main">
    <!-- Home Content Top Starts -->


                <?php
                    while ( have_posts() ) : the_post();

                        get_template_part( 'template-parts/content', 'page' );

                        // If comments are open or we have at least one comment, load up the comment template.
                        /*if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif; */

                    endwhile; // End of the loop.
                ?>


</div>

<!-- Footer -->
<?php get_footer(); ?>
