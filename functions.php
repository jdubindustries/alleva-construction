<?php
/**
 * socius_custom functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package socius_custom
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function socius_custom_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on socius_custom, use a find and replace
	 * to change 'socius_custom' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'socius_custom', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'main-menu' => __( 'Main Menu'),
		'top-menu' => __( 'Top Menu'),
		'footer-menu' => __( 'Footer Menu'),
	));


	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	));

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'socius_custom_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ));

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

    // Add Custom Logo
    add_theme_support( 'custom-logo', array(
		'flex-width' => true,
	));

}
add_action( 'after_setup_theme', 'socius_custom_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
if ( ! function_exists( 'socius_custom_content_width' ) ) :
	function socius_custom_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'socius_custom_content_width', 640 );
	}
endif;
add_action( 'after_setup_theme', 'socius_custom_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

function socius_custom_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Top', 'socius_custom' ),
		'id'            => 'sidebar-top',
		'description'   => esc_html__( 'Add widgets here.', 'socius_custom' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
	));
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Bottom', 'socius_custom' ),
		'id'            => 'sidebar-bot',
		'description'   => esc_html__( 'Add widgets here.', 'socius_custom' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
	));
	register_sidebar( array(
		'name'          => esc_html__( 'Internal CTA', 'socius_custom' ),
		'id'            => 'sidebar-internal',
		'description'   => esc_html__( 'Add widgets here.', 'socius_custom' ),
		'before_widget' => '<div class="col-md-6"><section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section></div>',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
	));
}
add_action( 'widgets_init', 'socius_custom_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

function socius_custom_scripts() {
	if ( ! is_admin() ) {
        // deregister the original version of jQuery
        wp_deregister_script('jquery');
        // register it again, this time with no file path
        wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-1.12.4.min.js');
        // add it back into the queue
        wp_enqueue_script('jquery');
    }

	//Theme Files
	wp_enqueue_script( 'vendor-js', get_template_directory_uri() . '/js/vendors.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/js/custom.min.js', array('jquery'), '', true );
    wp_enqueue_style( 'theme-css', get_template_directory_uri() . '/css/vendors.min.css');
	wp_enqueue_style( 'vendor-css', get_template_directory_uri() . '/css/style.min.css');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'socius_custom_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

// Remove jQuery Migrate Script from header and Load jQuery from Google API

function crunchify_stop_loading_wp_embed_and_jquery() {
	if (!is_admin()) {
		wp_deregister_script('wp-embed');
		wp_deregister_script('wp-emoji-release');
	}
}
add_action('init', 'crunchify_stop_loading_wp_embed_and_jquery');

//Remove script versioning

function _remove_script_version( $src ){
    if ( strpos( $src, 'ver=' ) ) {
                    $src = remove_query_arg( 'ver', $src );
    }
    return $src;
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

//Remove Emoji loading
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

//Remove Customizer from Admin

function wpse200296_before_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('customize');
}
add_action( 'wp_before_admin_bar_render', 'wpse200296_before_admin_bar_render' );

$role = get_role( 'editor' );
$role->add_cap( 'edit_theme_options' );
$role->add_cap( 'manage_options' );

/* Clean up the admin sidebar navigation *************************************************/

function remove_admin_menu_items() {
    if( !(current_user_can( 'activate_plugins' )) ) {
        $remove_menu_items = array(__('Links'),__('Media'), __('Comments'), __('Tools'), __('Settings'), __('CPT'));
        global $menu;
        end ($menu);
        while (prev($menu)){
                        $item = explode(' ',$menu[key($menu)][0]);
                        if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
                        unset($menu[key($menu)]);}
        }
        //remove_submenu_page( 'themes.php', 'widgets.php' );
        remove_submenu_page( 'themes.php', 'themes.php');
        unset($menu['themes.php'][5]); // Removes 'Themes'.
    }
}
add_action('admin_menu', 'remove_admin_menu_items');

//Changes the default Wordpress logo to the client logo. Make sure you change the background size, width, and height depending on the logo size.

function my_login_head() {
    echo "
    <style>
    body.login #login h1 a {
        background: url('".get_bloginfo('template_url')."/images/logo.png') no-repeat scroll center top transparent;
        width:344px;
        height: 95px;
        background-size: 344px 95px;
        margin: 0 auto;
    }
    </style>
    ";
}
add_action("login_head", "my_login_head");
remove_action('wp_head', 'wp_generator');


function my_login_logo_url () {
    return home_url();
}
add_filter('login_headerurl', 'my_login_logo_url');


function blank_version() {
    return '';
}
add_filter('the_generator','blank_version');


if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
    //'page_title'  => 'Company Information',
    'menu_title'  => 'Company Info',
    'menu_slug'   => 'company-information',
    'icon_url' 	  => 'dashicons-id', // Add this line and replace the second inverted commas with class of the icon you like
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Home Page CTAs',
	// 	'menu_title'	=> 'Home Page CTAs',
	// 	'parent_slug'	=> 'theme-general-settings',
  // ));

	// acf_add_options_page(array(
  //   'page_title'  => 'Theme Options',
  //   'menu_slug'   => 'theme-options',
  //   'capability'  => 'edit_posts',
  //   'redirect'    => false
  // ));

	acf_add_options_page(array(
		'page_title' 	=> 'Promotions',
		'menu_title'	=> 'Promotions',
		'menu_slug'	=> 'promotions-new',
		'icon_url' 	  => 'dashicons-tickets-alt',
		'capability'  => 'edit_posts',
		'redirect'    => false
	));

  acf_add_options_page(array(
    'page_title'  => 'Home Page CTAs',
    'menu_title'  => 'Home Page CTAs',
    'menu_slug'   => 'homepage-ctas',
    'icon_url' 	  => 'dashicons-megaphone', // Add this line and replace the second inverted commas with class of the icon you like
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));

	acf_add_options_page(array(
		'page_title' 	=> 'Home Makeovers Gallery',
		'menu_title'	=> 'Home Makeovers Gallery',
		'menu_slug'	=> 'homemakovers',
		'icon_url' 	  => 'dashicons-format-gallery',
		'capability'  => 'edit_posts',
		'redirect'    => false
  ));

  acf_add_options_page(array(
    'page_title'  => 'Product Galleries',
    'menu_title'  => 'Product Galleries',
    'menu_slug'   => 'product-galleries',
    'icon_url' 	  => 'dashicons-camera', // Add this line and replace the second inverted commas with class of the icon you like
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));

	acf_add_options_page(array(
    'menu_title'  => '3 Steps',
    'menu_slug'   => 'three-steps',
    'icon_url' 	  => 'dashicons-clipboard', // Add this line and replace the second inverted commas with class of the icon you like
    'capability'  => 'edit_posts',
    'redirect'    => true
  ));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Kitchens - Settings',
		'menu_title'	=> 'Kitchens',
		'parent_slug'	=> 'three-steps',
  ));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Baths - Settings',
		'menu_title'	=> 'Baths',
		'parent_slug'	=> 'three-steps',
  ));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Masonry - Settings',
		'menu_title'	=> 'Masonry',
		'parent_slug'	=> 'three-steps',
  ));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Additions - Settings',
		'menu_title'	=> 'Additions',
		'parent_slug'	=> 'three-steps',
  ));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Repairs - Settings',
		'menu_title'	=> 'Repairs',
		'parent_slug'	=> 'three-steps',
  ));

  acf_add_options_page(array(
    'page_title'  => 'Reviews',
    'menu_title'  => 'Reviews',
    'menu_slug'   => 'testimonials',
    'icon_url' 	  => 'dashicons-testimonial', // Add this line and replace the second inverted commas with class of the icon you like
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));

	acf_add_options_page(array(
		'page_title' 	=> 'Trust & CTA',
		'menu_title'	=> 'Trust & CTA',
		'menu_slug'	=> 'trust-images',
		'icon_url' 	  => 'dashicons-shield',
		'capability'  => 'edit_posts',
		'redirect'    => false
  ));

}

// Toolbar Theme Options

function create_smtheme_menu() {
	global $wp_admin_bar;
	$menu_id = ['smtheme-company', 'smtheme-settings'];
	$wp_admin_bar->add_menu(array('id' => $menu_id[0], 'title' => __('Company Information'), 'href' => ''. get_bloginfo('url') .'/wp-admin/admin.php?page=company-information'));
	//$wp_admin_bar->add_menu(array('id' => $menu_id[1], 'title' => __('Products & Services'), 'href' => ''. get_bloginfo('url') .'/wp-admin/admin.php?page=product-services'));
}
add_action('admin_bar_menu', 'create_smtheme_menu', 2000);

// Clean HEAD tag links

remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version


// Registering Additional Theme Image Sizes

add_theme_support( 'post-thumbnails' );

// Add featured image sizes
add_image_size( 'promotion', 1920, 645, true ); // width, height, crop
add_image_size( 'hero-internal', 1920, 525, true ); // width, height, crop
add_image_size( 'gallery', 1155, 685, true ); // width, height, crop
add_image_size( 'gallery-about', 700, 485, true ); // width, height, crop
add_image_size( 'gallery-thumb', 125, 125, true ); // width, height, crop
add_image_size( 'makeover-gallery', 1200, 539, true ); // width, height, crop
add_image_size( 'trust', 225, 135, false ); // width, height, crop
add_image_size( 'home-cta', 335, 510, true ); // width, height, crop
// add_image_size( 'content-home', 950, 335, true ); // width, height, crop
// add_image_size( 'industry', 290, 162, true ); // width, height, crop
// add_image_size( 'industry-icon', 200, 200, true ); // width, height, crop
// add_image_size( 'sub-industry', 286, 118, true ); // width, height, crop
// add_image_size( 'structure', 286, 138, true ); // width, height, crop
// add_image_size( 'structurecover', 1920, 860, true ); // width, height, crop
// add_image_size( 'accessories', 286, 286, true ); // width, height, crop
// add_image_size( 'team', 286, 286, true ); // width, height, crop
// add_image_size( 'profile', 360, 450, true ); // width, height, crop
// add_image_size( 'content-cta', 460, 386, true ); // width, height, crop
// add_image_size( 'study', 500, 270, true ); // width, height, crop
// add_image_size( 'casestudy', 500, 315, true ); // width, height, crop

//Admin Styles
function admin_styles() {
	wp_enqueue_style('admin_styles' , get_template_directory_uri().'/css/admin.css');
}

add_action('admin_head', 'admin_styles');

//Shortcodes

//[home url]
function homeurl_func( $atts ){
	$home = home_url();
	return "$home";
}
add_shortcode( 'wphome', 'homeurl_func' );
add_filter('widget_text','do_shortcode');

//hook into the init action and call create_casestydy_taxonomies when it fires
add_action( 'init', 'create_casestudies_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it casestudies for your posts

function create_casestudies_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

  // $labels = array(
  //   'name' => _x( 'Case Studies', 'taxonomy general name' ),
  //   'singular_name' => _x( 'Case Study', 'taxonomy singular name' ),
  //   'search_items' =>  __( 'Search Case Studies' ),
  //   'all_items' => __( 'All Case Studies' ),
  //   'parent_item' => __( 'Parent Case Study' ),
  //   'parent_item_colon' => __( 'Parent Case Study:' ),
  //   'edit_item' => __( 'Edit Case Study' ),
  //   'update_item' => __( 'Update Case Study' ),
  //   'add_new_item' => __( 'Add New Case Study' ),
  //   'new_item_name' => __( 'New Case Study Name' ),
  //   'menu_name' => __( 'Case Studies' ),
  // );

// Now register the taxonomy

  // register_taxonomy('casestudies',array('post'), array(
  //   'hierarchical' => true,
  //   'labels' => $labels,
  //   'show_ui' => true,
  //   'show_admin_column' => true,
  //   'query_var' => true,
  //   'show_in_nav_menus' => true,
		// 'show_tagcloud' => true,
		// 'meta_box_cb' => false, //remove meta box
	 //    'rewrite' => array( 'slug' => 'topic' ),
	 //  ));
}

// Get Current Template
// add_action( 'admin_bar_menu', 'show_template' );
// 	function show_template() {
// 	global $template;
// 	print_r( $template );
// }
