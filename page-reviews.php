<?php
/**
 * Template Name: Reviews
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

<!-- Main Content -->

	<div id="main-content" class=" container" role="main">


				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'template-parts/content', 'page' ); ?>

				<?php endwhile; // End of the loop. ?>

				<?php if(get_field('testimonial', 'option')):

		            while(the_repeater_field('testimonial', 'option')):
		            $location = get_sub_field('location'); ?>
		                <p class="testPageBody"><?php echo get_sub_field('testimonial_text') ?>
		                <br />
		                <span class="testPageInfo">
		                    <b><?php echo get_sub_field('testimonial_name'); ?></b> <?php if ($location): ?> | <?php echo $location; endif; ?> 
		                </span>
		                </p>
		            <?php endwhile; ?>
	            <?php endif; ?>

	</div>

<!-- Footer -->
<?php get_footer(); ?>
